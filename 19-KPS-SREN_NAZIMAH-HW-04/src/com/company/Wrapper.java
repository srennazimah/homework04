package com.company;

import java.util.*;

public class Wrapper<N> {
    private List<N> list = new ArrayList<>();
    private N number;

    private Set<N> hashSet = new HashSet<N>();

    //method to set arrayList
    public void addItem(N number) {

        if (number == null) {
            throw new NullPointerException();
        } else
            list.add(number);
    }
    //method to get all arrayList
    public void getItem(int index) {
        for (N li : list) {
            if (hashSet.add(li) == true)
                System.out.println(li);
        }
    }
}
