package com.company;

public class Main {

    private static int i;

    public static void main(String[] args) {
        // write your code here
        Wrapper<Integer> wrapper = new Wrapper<>();

        try {
            wrapper.addItem(5);
            wrapper.addItem(3);
            wrapper.addItem(3);
            wrapper.addItem(2);
            wrapper.addItem(4);
            wrapper.addItem(4);
            wrapper.addItem(4);
            wrapper.addItem(null);

        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("\n List all from inputted");
        wrapper.getItem(i);
    }
}

