package com.company;

class Main{
    public static void main(String[] args) {
        // write your code here
        Message[] message = new  Message[7];
        message[0]=new Message("Hello KSHRD!\n",400);
        message[1]=new Message("************************************\n",250);
        message[2]=new Message("I will try my best to be here at HRD.\n",250);
        message[3]=new Message("------------------------------------\n",250);
        message[4]=new Message("Downloading",10);
        message[5]=new Message("...........",250);
        message[6]=new Message("complete 100%",10);

        MessageList myThread2 = new MessageList(message);
        myThread2.start();
    }
    synchronized static void print(Message[] myThread1){
        for (Message m:myThread1){
            char[] ch = new char[(m.getMs()).length()];

            for (int i = 0; i < (m.getMs()).length(); i++) {
                ch[i] = (m.getMs()).charAt(i);
            }
            for (char c : ch) {
                try {
                    Thread.sleep(m.getSecond());
                } catch (Exception e) {
                }
                System.out.print(c);
            }
        }
    }
}